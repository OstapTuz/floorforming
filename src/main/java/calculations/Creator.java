package calculations;

import model.Point;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Creator {
    static Scanner scanner = new Scanner(System.in);
    public static List<Point> points = new LinkedList<>();

    public static void createFromConsole() {  // console format
        System.out.println("Please input points\n------------------");
        int iterator = 1;
        while (true) {
            System.out.println("Point #" + iterator++);
            System.out.print("Input x: ");
            int x = scanner.nextInt();
            System.out.print("Input y: ");
            int y = scanner.nextInt();
            points.add(new Point(x, y));
            System.out.println("------------------");
            if (points.size() >= 4) {
                System.out.println("Do you want add one more? (y/n)");
                String stayInput = scanner.next();
                if (stayInput.equals("n")) break;
            }
        }
    }

    public static void createFromFile(File file) throws IOException {
        Scanner scanner = new Scanner(file);

        while (scanner.hasNext()) {
            String[] pointData = scanner.nextLine().split(",");
            points.add(new Point(Integer.parseInt(pointData[0]), Integer.parseInt(pointData[1])));
        }
    }
}
