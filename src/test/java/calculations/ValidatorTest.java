package calculations;

import exceptions.InvalidArgumentsException;
import model.Point;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static calculations.Creator.points;
import static calculations.Validator.validate;

public class ValidatorTest {
    @Test
    public void validateTest() throws InvalidArgumentsException {
        points.add(new Point(0,0));
        points.add(new Point(0,10));
        points.add(new Point(10,10));
        points.add(new Point(10,0));
        Assertions.assertDoesNotThrow(Validator::validate);
        points.clear();
    }
}
