package webcontent;

import exceptions.InvalidArgumentsException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class Application {
    public static void main(String[] args) throws InvalidArgumentsException, IOException {
        SpringApplication.run(Application.class, args);
    }
}
