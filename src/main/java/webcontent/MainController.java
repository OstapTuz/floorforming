package webcontent;

import exceptions.InvalidArgumentsException;
import model.Point;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

import static calculations.Creator.points;
import static calculations.Validator.validate;
import static java.lang.Integer.parseInt;

@Controller
public class MainController {
    @PostMapping("/add")
    public String addNewPoint(@RequestParam String x, @RequestParam String y) {
        points.add(new Point(parseInt(x), parseInt(y)));
        System.out.println(points);
        return "index";
    }

    @PostMapping("/result")
    public String submitData(Model model) throws InvalidArgumentsException, IOException {
        validate();
        model.addAttribute("points", points);
        return "result";
    }

    @PostMapping("/home")
    public String goHome() {
        points.clear();
        return "index";
    }
}
