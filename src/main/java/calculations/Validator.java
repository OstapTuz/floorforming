package calculations;

import exceptions.InvalidArgumentsException;
import model.Line;
import model.Point;

import java.util.ArrayList;
import java.util.List;

import static calculations.Creator.points;

public class Validator {
    private static int MINIMUM_POINTS = 4;

    public static void validate() throws InvalidArgumentsException {
        validateMinimumPoints();
        validateRightCorners();
        validateCloseSpace();
        validateCross();
        System.out.println("Validation is successful");
        System.out.println(points);
    }

    private static void validateMinimumPoints() throws InvalidArgumentsException {
        if (points.size() < MINIMUM_POINTS)
            throw new InvalidArgumentsException("Please, add points!!! Should be at least " + MINIMUM_POINTS);
    }

    private static void validateRightCorners() throws InvalidArgumentsException {
        for (int i = 0; i < points.size() - 1; i++) {
            Point p1 = points.get(i);
            Point p2 = points.get(i + 1);
            if (p1.getX() != p2.getX() && p1.getY() != p2.getY()) {
                throw new InvalidArgumentsException("Illegal room, Wall between " + points.get(i) + " and " + points.get(i + 1) + " is diagonal");
            }
        }
    }

    private static void validateCloseSpace() throws InvalidArgumentsException {
        int last = points.size() - 1;
        if (points.get(0).getX() != points.get(last).getX() && points.get(0).getY() != points.get(last).getY()) {
            throw new InvalidArgumentsException("Points dont create close space");
        }
    }

    private static void validateCross() throws InvalidArgumentsException {
        int last = points.size() - 1;
        List<Line> lines = new ArrayList<>();
        for (int i = 0; i < points.size() - 1; i++) {
            lines.add(new Line(points.get(i), points.get(i + 1)));
        }
        lines.add(new Line(points.get(last), points.get(0)));

        for (int i = 0; i < lines.size() - 1; i++) {
            for (int j = i + 1; j < lines.size(); j++) {
                if (lines.get(i).isCrossedLine(lines.get(j))) {
                    throw new InvalidArgumentsException("Lines " + lines.get(i) + " and " + lines.get(j) + " are intersected");
                }
            }
        }
    }
}
