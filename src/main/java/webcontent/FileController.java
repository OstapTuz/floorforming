package webcontent;

import exceptions.InvalidArgumentsException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static calculations.Creator.createFromFile;
import static calculations.Creator.points;
import static calculations.Validator.validate;

@Controller
public class FileController {

    @RequestMapping(value = "/readFile", method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String fileUpload(@RequestParam("file") MultipartFile file, Model model) throws IOException, InvalidArgumentsException {
        File convertFile = new File("data/" + file.getOriginalFilename());
        convertFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(convertFile);
        fout.write(file.getBytes());
        fout.close();
        createFromFile(convertFile);
        validate();
        model.addAttribute("points", points);
        return "result";
    }

}
