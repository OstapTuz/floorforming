package exceptions;

public class InvalidArgumentsException extends Exception {
    private String message;

    public InvalidArgumentsException() {
    }

    public InvalidArgumentsException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
