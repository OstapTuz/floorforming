package model;

public class Line {
    private Point start;
    private Point end;

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    public boolean isCrossedLine(Line line) {
        double Ua, Ub, numeratorA, numeratorB, denominator;
        int x1 = this.start.getX();
        int y1 = this.start.getY();
        int x2 = this.end.getX();
        int y2 = this.end.getY();
        int x3 = line.start.getX();
        int y3 = line.start.getY();
        int x4 = line.end.getX();
        int y4 = line.end.getY();

        denominator = (y4 - y3) * (x1 - x2) - (x4 - x3) * (y1 - y2);
        if (denominator == 0) {
            if ((x1 * y2 - x2 * y1) * (x4 - x3) - (x3 * y4 - x4 * y3) * (x2 - x1) == 0 && (x1 * y2 - x2 * y1) * (y4 - y3) - (x3 * y4 - x4 * y3) * (y2 - y1) == 0)
                return true;
            else return false;
        } else {
            numeratorA = (x4 - x2) * (y4 - y3) - (x4 - x3) * (y4 - y2);
            numeratorB = (x1 - x2) * (y4 - y2) - (x4 - x2) * (y1 - y2);
            Ua = numeratorA / denominator;
            Ub = numeratorB / denominator;
            if (Ua > 0 && Ua < 1 && Ub > 0 && Ub < 1) return true;
            else return false;
        }
    }

    @Override
    public String toString() {
        return "[" + start + " - " + end + " ]";
    }
}
