package model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LineTest {
    @Test
    public void isCrossedLineTest(){
        Line line = new Line(new Point(0,0), new Point(0, 10));
        Line crossedLine = new Line(new Point(0,0), new Point(0, 10));
        Assertions.assertTrue(line.isCrossedLine(crossedLine));
    }
}
